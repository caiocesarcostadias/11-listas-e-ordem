package atividade;

import java.util.Comparator;

public class Relogio implements Comparator<Relogio> {

	private HorarioNG hms;
	private Data dma;

	public Relogio(HorarioNG hms, Data dma) {
		this.hms = new HorarioNG(hms);
		this.dma = dma;
	}

	public void tictac() {

		hms.incrementaSegundo();

		if (hms.ehPrimeiroHorario()) {
			dma.incrementaDia();
		}
	}

	@Override
	public String toString() {
		return dma + " " + hms;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Relogio other = (Relogio) obj;
		if (dma == null) {
			if (other.dma != null)
				return false;
		} else if (!dma.equals(other.dma))
			return false;
		if (hms == null) {
			if (other.hms != null)
				return false;
		} else if (!hms.equals(other.hms))
			return false;
		return true;
	}

	public boolean menorOuIgual(HorarioNG horario, Data data) {
		if (this.dma.menorOuIgual(data) && this.hms.menorOuIgual(horario))
			return true;
		return false;
	}

	public boolean maiorOuIgual(HorarioNG horario, Data data) {
		if (this.dma.maiorOuIgual(data) && this.hms.maiorOuIgual(horario))
			return true;
		return false;
	}

	public boolean maior(HorarioNG horario, Data data) {
		if (this.dma.maior(data) && this.hms.maior(horario))
			return true;
		return false;
	}

	@Override
	public int compare(Relogio o1, Relogio o2) {
		if (o1.dma.equals(o2.dma) && o1.hms.equals(o2.hms)) {
			return 0;
		}

		return o1.dma.maior(o2.dma) && o1.hms.maior(o2.hms) ? 1 : -1;
	}
}
